#!/bin/bash

#Basic Server Setup Script

echo "Username?"
read username
echo "Password?"
read -s password
echo "Hostname?"
read hostname 
echo Running Updates..
yum -y update
echo Adding User...
useradd $username
echo Setting Password...
echo $password | passwd --stdin $username
gpasswd -a $username wheel
echo Installing EPEL repository...
yum -y install epel-release
echo Installing Basic Programs...
yum -y install htop sysstat fail2ban nano
echo Setting Hostname...
hostnamectl set-hostname $hostname
echo Disabling Root Login...
sed -i -e 's/#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
echo Rebooting
reboot