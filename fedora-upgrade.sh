#!/bin/bash
### Start and Enable Firewall ###
systemctl start firewalld
systemctl enable firewalld

### Cockpit ###
sudo dnf install cockpit cockpit-machines
sudo systemctl enable --now cockpit.socket
sudo firewall-cmd --add-service=cockpit --permanent

### Upgrade Fedora ###
sudo dnf -y upgrade --refresh
sudo dnf -y install dnf-plugin-system-upgrade
sudo rpm --import https://getfedora.org/static/F5282EE4.txt
sudo dnf system-upgrade download -y --refresh --releasever=27
sudo dnf system-upgrade reboot

### Install virtualization Group ###
su -c "dnf install @virtualization"
su -c "systemctl start libvirtd"
su -c "systemctl enable libvirtd"

https://fedoraproject.org/wiki/AutoUpdates#Install_and_settings_of_dnf-automatic


