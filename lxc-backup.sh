
#!/bin/bash
echo
echo "=============================="
echo Stopping Containers.....
echo "=============================="
echo
for i in $(ls /var/lib/lxc); do lxc-stop -n $i ; done
echo Done!
echo
echo "=============================="
echo Backing Up Containers.....
echo "=============================="
echo
cd /var/lib/lxc/
for i in $(ls /var/lib/lxc); do tar --numeric-owner -czf /root/backups/lxc/$i.tgz $i ; done
echo Done!
echo
echo "=============================="
echo Starting Containers.....
echo "=============================="
echo
for i in $(ls /var/lib/lxc); do lxc-start -n $i ; done
echo Done!
echo
echo "=============================="
echo Syncing Container Backups to B2.....
echo "=============================="
echo
b2 sync --keepDays 30 --replaceNewer /root/backups/lxc b2://aaronstuder/backups/lxc
echo Done!
echo
echo "=====Backup Complete!====="

